#!/bin/sh

#Config
output_folder="tmp"
r_to_fastq="scripts/rdata_to_fastq.R"
qc_script="scripts/get_quality.py"
plot_script="scripts/plot_qc.py"
sankey_diagram="scripts/sankeyNetwork.R"
report="scripts/Report.Rmd"
knit_report="scripts/knit_report.R"

loc_timmed_data="trimmed_sludge.fastq"
basecall_output="chiron_all.fastq"
centrifuge_output="centrifuge_output.tsv"
centrifuge_report="centrifuge_report.tsv"
location_kreport="centrifuge_kraken.report"
krona_output="kraken.html"
sankey_output="sankey_diagram.html"
QC_get_quality_output="fastqc_quality.json"

#Variables
DATA=""
CENTRIFUGE_DB=""
QUALITY=0
QUALITY_CHECK=0
CORE_USE=1
TYPE="fastq"
MAX_DEPTH=5
MAX_ITEMS_PER_DEPTH=10
SAVE=0

#help
help_func(){
	usage="pipeline.sh [-h] [-Q]  [-s <save_folder>] [-d <db_loc>] [-q <quality>] [-C <cores>] [--data-type <fast5/rdata>] [-D <max_depth>] [-a <max_items_per_depth>] <data_file>\n"
	arguments="
	where:\n\t
		-h\thelp\n\t
		-Q\tQuality check uitvoeren\n\t
		-s\tSave the intermediate data from the tools and the folder in witch to save the data\n\t
		-d\tLocation to the centrifuge database\n\t
		-q\tMinimum quality op the reads\n\t
		-C\tNumber of cores; default=1\n\t
		--data-type\tThe type of data (fast5/rdata/fastq); default=fastq\n\t
		-D\tThe maximum depth for the sankey diagram; default=5\n\t
		-a\tThe maximum amount of items per depth in the sankey diagram; default=10\n\t
		"

	echo $usage
	echo $arguments
	exit

}

#Basecalling 
basecalling(){
	# Executing the basecall
	chiron call -i $DATA -o $output_folder/chrion_output
	
	# Turning the output in one file
	for f in $output_folder/chiron_output/*/*.fastq; do cat "$f" >>  $basecall_output ; done
	
	# Setting the new file as DATA
	DATA=$basecall_output
}

#Rdata omzetten naar fastq data
rdataToFastq(){
	Rscript $r_script -f $DATA > ""
	DATA="$output_folder/sludge.2d.fastq"
}

#Quality controll fastq data
qcFastq(){
	python3 $qc_script -f $DATA -o $output_folder/$1_$QC_get_quality_output
	python3 $plot_script -f $output_folder/$1_$QC_get_quality_output -n $1 -o $output_folder
}

#Trim en Filter de data
trim(){
	NanoFilt -q $QUALITY $DATA > $output_folder/$loc_timmed_data
	DATA=$output_folder/$loc_timmed_data

	echo "Quality check trimmed data"
	qcFastq "trimmed"
}

#identificatie
id_organisme(){
	centrifuge -x $CENTRIFUGE_DB -p $CORE_USE $output_folder/$loc_timmed_data -S $output_folder/$centrifuge_output --report-file $output_folder/$centrifuge_report
}

#Visualisatie
visualisatie(){
	#Centrifuge data omzeten naar kraken data
	centrifuge-kreport -x $CENTRIFUGE_DB $output_folder/$centrifuge_output > $output_folder/$location_kreport

	#Krona
	ktImportTaxonomy -m 3 -t 5 $output_folder/$location_kreport -o $$krona_output

	#Sankey diagram
	Rscript $sankey_diagram -f $output_folder/$location_kreport -d $MAX_DEPTH -a $MAX_ITEMS_PER_DEPTH -o $sankey_output

}

#Raportage
raportage(){
	Rscript $knit_report -b ../$output_folder/quality_per_position_head_raw.png -B ../$output_folder/quality_per_position_tail_raw.png -a ../$output_folder/quality_per_position_head_trimmed.png -A ../$output_folder/quality_per_position_tail_trimmed.png -j ../$output_folder/raw_fastqc_quality.json -J $output_folder/trimmed_fastqc_quality.json -t ../$sankey_output.Rdata -k $krona_output -f $report
}


###
#main
###

#Argument parsen
while [ "$#" -gt "0" ]
do
	case $1 in
		"-h")
			help_func ""
			;;
		"-d")
			CENTRIFUGE_DB="$2"
			;;
		"-D")
			MAX_DEPTH=$2
			;;
		"-a")
			MAX_ITEMS_PER_DEPTH=$2
			;;
		"-q")
			QUALITY=$2
			;;
		"-Q")
			QUALITY_CHECK=1
			;;
		"-C")
			CORE_USE=$2
			;;
		"-s")
			SAVE=1
			output_folder=$2
			;;
		"--data-type")
			case $2 in
				"fast5")
				TYPE="fast5"
				;;

				"rdata")
				TYPE="rdata"
				;;
				"fastq")
				TYPE="fastq"
				;;
				"$*")
				echo  "Invalid data_type given"
				;;
				esac
			;;
		"$*")
			DATA="$1"
			;;
	esac
	shift
done

mkdir $output_folder

#Basecalling
if [ $TYPE = "fast5" ]
then
	echo "basecaling the data"
	basecalling ""
fi

#Rdata omzetten in fastq data
if [ $TYPE = "rdata" ]
then
	echo "Rdata Omzetten in fastq data"
	rdataToFastq ""
fi

#Quality bepalen van de fastq

if [ $QUALITY_CHECK = 1 ]
then
	echo "Quality check"
	qcFastq "raw"
fi

#Trim en filter de data
echo "Fastq data trimmen en filteren"
trim ""

#Identificeer de organismen
echo "identificeren van organismen"
id_organisme ""

#Plot de organismen
echo "Ploting the organisms"
visualisatie ""

#Rapporteer de resultaten
echo "Rapporteer de resultaten"
raportage ""

if [ $SAVE = 0 ]
then
	rm -r $output_folder
fi 
