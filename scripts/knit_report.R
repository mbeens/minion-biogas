# Creates a tree like the Pavian tools does
# Author = Groepje Ronald
# Version = 1.0
# Status = Gereed voor gebruik

# Load packages
library(optparse)
library(rmarkdown)

# Create the commandline parser
option_list <- list(make_option(c("-j", "--QC_json_BFT"), type="character", default="../tmp/raw_fastqc_quality.json", help="Location of the QC json file before trim"),
                    make_option(c("-J", "--QC_json_AFT"), type="character", default="../tmp/raw_fastqc_quality.json", help="Location of the QC json file after trim"),
                    make_option(c("-b", "--QC_Head_BFT"), type="character", default="../tmp/quality_per_position_head_raw.png", help="Location of the head after trim image"),
                    make_option(c("-B", "--QC_Tail_BFT"), type="character", default = "../tmp/quality_per_position_head_raw.png", help = "Location of the tail before trim image"),
                    make_option(c("-a", "--QC_Head_AFT"), type="character", default = "../tmp/quality_per_position_head_raw.png", help = "Location of the head after trim image"),
                    make_option(c("-A", "--QC_Tail_AFT"), type="character", default = "../tmp/quality_per_position_head_raw.png", help = "Location of the tail after trim image"),
                    make_option(c("-t", "--tax_data"), type="character", default = "../tmp/sankey_diagram.html.Rdata", help = "The save location of the output"),
                    make_option(c("-k", "--krona_plot"), type="character", default = "../tmp/kraken.html", help = "The location "),
                    make_option(c("-f", "--file"), type="character", default = "Report.Rmd", help = "Location of the report script"),
                    make_option(c("-w", "--working_dir"), type="character", default = ".", help = "the current working directory"))

opt_parser <- OptionParser(option_list=option_list)
args <- parse_args(opt_parser)
parameters <- list(QC_Head_BFT = args$QC_Head_BFT, QC_Tail_BFT = args$QC_Tail_BFT, QC_Head_AFT = args$QC_Head_AFT, QC_Tail_AFT = args$QC_Tail_AFT, tax_data = args$tax_data, krona_plot = args$krona_plot)
render(args$file, params=parameters, output_file="multigenomics_report.html", output_dir=getwd())
