#! usr/bin/env python3
"""
Scipt that gets the quality scores from the json-file
and plots the results
"""

__author__ = 'Groepje Ronald'
__status__ = 'Gereed voor Gebruik'
__version__ = 'v1.0'

# imports
import json
import argparse
import matplotlib.pyplot as plt


def plot_avg_qualty_per_pos(file, step, head, name, output):
    """
    Makes a plot of the json file
    :type file: String
    :param file: The location of the json data file

    :type step: Integer
    :param step: The step size that is used to construct the plot.

    :type head: bool
    :param head: Is a bool value that indicates if is is the head data or tail data
    
    :type name: String
    :param name: The name of the graph

    :type output: String
    :param output: The output folder where the plots need to be saved
    """
    with open(file) as json_data:
        raw_data = json.load(json_data)
    if head:
        data = raw_data["quality_per_pos_head"]
        output = "{}/quality_per_position_head_{}.png".format(output, name)
    else:
        data = raw_data["quality_per_pos_tail"]
        output = "{}/quality_per_position_tail_{}.png".format(output, name)

    fig, ax = plt.subplots()
    x_pos = []
    y_pos = []
    for pos in range(1, len(data), step):
        x_pos.append(pos)

        range_list = [data[str(index)] for index in range(pos, pos+step)
                      if str(index) in data.keys()]
        y_pos.append(sum(range_list)/len(range_list))

    ax.plot(x_pos, y_pos)
    ax.set(xlabel="Positie (per {} bp)".format(step), ylabel="kwaliteit ()",
           title="gemiddelde kwaliteit per positie")
    ax.grid()
    fig.savefig(output)


def tail_head_trim(file, minimum):
    """
    Used to trimm the head and tail of the data
    :type file: String
    :param file: The location of the json data file

    :type minimum: Integer
    :param minimum: The minimum quality for each position
    """
    with open(file) as json_data:
        raw_data = json.load(json_data)

    head_tail_data = [raw_data["quality_per_pos_head"], raw_data["quality_per_pos_tail"]]

    head_pos = 0
    tail_pos = 0
    is_first_over_min = False

    for data in head_tail_data:
        for pos in range(1, len(data)):
            if data[str(pos)] > minimum and not is_first_over_min:
                head_pos = pos
                is_first_over_min = True
                break
        if is_first_over_min:
            break
    return head_pos, tail_pos


def main():
    """Main gets arguments and executes the functions"""
    parser = argparse.ArgumentParser()
    parser.add_argument("-f", "--file", type=str, default="tmp/fastqc_quality2.json",
                        help="The json file that contains the average quality per position")
    parser.add_argument("-s", "--step", type=int, default=100,
                        help="The step that is used to construct the plot")
    parser.add_argument("-n", "--name", type=str, default="chiron",
                        help="Name is added after the file names")
    parser.add_argument("-o", "--output", type=str, default="tmp",
                        help="The folder where the output will be saved")
    args = parser.parse_args()

    head_pos, tail_pos = tail_head_trim(args.file, args.step, 7)
    plot_avg_qualty_per_pos(args.file, args.step, True, args.name, args.output)
    plot_avg_qualty_per_pos(args.file, args.step, False, args.name, args.output)

    print("Head trim position: {}\nTail trim position: {}".format(head_pos, tail_pos))


if __name__ == "__main__":
    exit(main())
