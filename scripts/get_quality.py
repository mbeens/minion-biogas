#! usr/bin/env python3
"""
Calculates the quality of the reads and saves 
them in a json file
"""

__author__ = 'Groepje Ronald'
__status__ = 'Gereed voor Gebruik'
__version__ = 'v1.0'

#imports
import json
import argparse


class Meanscore():
    """Class for the mean scores"""

    def __init__(self, som, number):
        """Saving the variables"""
        self.som = som
        self.number = number

    def __repr__(self):
        """Changed repr"""
        return "Mean: {}\nnumber: {}".format(self.som, self.number, )


def get_quality_per_position(file, output_file):
    """
    Calculates the quality of the reads per position and saves them in json
    :type file: String
    :param file: The location of fastq input file
    
    :type output_file: String
    :param output_file: The location of the output file
    """
    # The variables used where all the raw data wil be stored
    read_dict = {"quality_per_pos_head": {}, "length": [], "quality_per_pos_tail": {}}
    length = read_dict["length"]
    quality_per_pos_head = read_dict["quality_per_pos_head"]
    quality_per_pos_tail = read_dict["quality_per_pos_tail"]
    num_of_reads = 0
    min_length = 0
    max_length = 0

    # Open the fastq file
    with open(file) as data:

        # Loop through the fastq data
        for index_r, read in enumerate(data):

            # check if the current line is a quality line
            if (index_r - 3) % 4 == 0:
                num_of_reads += 1
                # Add the length to the dictionary
                read_length = len(read)
                if read_length < min_length or min_length == 0:
                    min_length = read_length
                if read_length > max_length:
                    max_length = read_length
                length.append(read_length)

                # Go through each quality and add the position and quality to the dictionary
                for index, quality in enumerate(read, 1):
                    if index not in quality_per_pos_head.keys():
                        quality_per_pos_head[index] = Meanscore(ord(quality) - 33, 1)
                    else:
                        mean_obj = quality_per_pos_head[index]
                        quality = ord(quality) - 33
                        mean_obj.som += quality
                        mean_obj.number += 1
                    quality = read[read_length-index]

                    if index not in quality_per_pos_tail.keys():
                        quality_per_pos_tail[index] = Meanscore(ord(quality) - 33, 1)
                    else:
                        mean_obj = quality_per_pos_tail[index]
                        quality = ord(quality) - 33
                        mean_obj.som += quality
                        mean_obj.number += 1

    # Create the variable that will be saved to the json file
    json_dict = {"quality_per_pos_head": {}, "min_length": 0,
                 "max_length": 0, "quality_per_pos_tail": {}}

    total_mean = []
    # Get the mean quality of each position
    for x in quality_per_pos_head:
        mean = quality_per_pos_head[x].som / quality_per_pos_head[x].number
        total_mean.append(mean)
        json_dict["quality_per_pos_head"][x] = mean
    total_mean = sum(total_mean) / len(total_mean)

    # Get the mean quality of each position
    for x in quality_per_pos_tail:
        mean = quality_per_pos_tail[x].som / quality_per_pos_tail[x].number
        json_dict["quality_per_pos_tail"][x] = mean

    # Get the minimum length of the reads
    json_dict["min_length"] = min(read_dict["length"])

    # Get the maximum length of the reads
    json_dict["max_length"] = max(read_dict["length"])

    json_dict["number_of_reads"] = num_of_reads
    json_dict["length_of_reads"] = [min_length, max_length]
    json_dict["Average_quality"] = total_mean

    print("Number of reads: {}".format(num_of_reads))
    print("Length of reads: {}-{} basepares".format(min_length, max_length))
    print("Average quality: {}".format(round(total_mean, 4)))
    with open(output_file, 'w') as outfile:
        json.dump(json_dict, outfile)


def main():
    """Main gets arguments and executes the functions"""
    parser = argparse.ArgumentParser()
    parser.add_argument("-f", "--file", type=str, default="../tmp/sludge.2d.fastq",
                        help="The fastq file that contains the reads")
    parser.add_argument("-o", "--output", type=str, default="../tmp/fastqc_quality2.json",
                        help="The output json file that is created")
    args = parser.parse_args()
    get_quality_per_position(args.file, args.output)


if __name__ == "__main__":
    exit(main())
