# minION Biogas pipeline 
This is a pipeline that identifies all the organisms in minion data. you can use 3 data types: fastq, RData and fast5.
when fast5 data is used the pipeline performs a basecalling (This can take a couple of days) on the data. 
And processes the basecalled data to find the organisms. 
When fastq or RData is used it skips the basecalling and immendiatly processes the data. 

Before you run the pipeline install the following tools:
NanoFilt, Chiron, Centrifuge, Krona and Pavian. 

### Installation

#### Installing the python packages
Our code is written with python 3.7.3. 
In this folder there is a file called requirements.txt this file contains the dependencies that our code has it also 
contains the tools NanoFilt and Chiron, and can be used in pip using the command:
```
pip3 install -r requitrements.txt
```

#### Installing the R packages
Our code is written with R 3.6.2. 
In this folder there is a file called installing_r_packages that when run will install all the r packages that we use
for our R scripts. This file can be run with the command:
~~~
Rscript install_r_packages.R
~~~

#### Installing the tools
**Centrifuge**  
To install centrifuge follow these steps  
1. Download the newest release from: https://github.com/infphilo/centrifuge/releases  
2. Unpack the downloaded file  
3. Open the folder in the terminal  
4. Execute the command
```bash
make install
```
If you are having problems with installing you can take a look at there instructions at the [Centrifuge manual](https://github.com/infphilo/centrifuge/blob/master/MANUAL.markdown) 

**NanoFilt**  
NanoFilt is a python package and is installed with requirements.txt and do not require specific actions you need to do 
except to check if you can run this program via the terminal.If this is not the case add it to PATH.

**Chiron**  
Chiron is also a python package  witch is installed with the requirements.txt. After this is run you will need to do 
one more thing, After chiron is installed with pip you will need to add it to path using the command.
~~~
export PYTHONPATH=[<Path to Chiron/Chiron>]:$PYTHONPATH
~~~

**Krona**  
To install the Krona tool type the following commands in the terminal.  
Download the Krona directory.
``` bash
curl -LOk https://github.com/marbl/Krona/releases/download/v2.7/KronaTools-2.7.tar
```

Untar the file.
``` bash
tar xvf KronaTools-2.7.tar
```

Change the directory to Krona.
``` bash 
cd KronaTools-2.7
```

Install Krona using Perl script.
``` bash
./install.pl
```

Then one databases have to be updated.
``` bash
updateTaxonomy.sh 
```

After you have installed all the tools you are able to run the pipeline. If this is not the case check if all the tools 
can be run in the terminal.

### The Pipeline
The pipeline can take input from 3 different files a FASTQ file, a RData file with fast5 summary objects and a folder 
with fast5 files. After you have run the pipeline you will have a report with the QC results and the phylogenetic trees.

**Usage**
``` bash
pipeline.sh [-h] [-Q]  [-s <save_folder>] [-d <db_loc>] [-q <quality>] [-C <cores>] [--data-type <fast5/rdata>] [-D <max_depth>] [-a <max_items_per_depth>] <data_file>
```
where:  
-h help  
-Q Quality check uitvoeren  
-s Save the intermediate data from the tools and the folder in witch to save the data  
-d Location to the centrifuge database  
-q Minimum quality op the reads  
-C Number of cores; default=1  
--data-type The type of data (fast5/rdata/fastq); default=fastq  
-D The maximum depth for the sankey diagram; default=5  
-a The maximum amount of items per depth in the sankey diagram; default=10  


### Tools
#### NanoFilt  
This tool can be used for trimming and filtering the data from Oxford Nanopore sequencing data. This tool is written in python3 and  
can be run in the terminal. The filtering can be done based on the minimal avarage quality of the read or on the minimal read length.  
After filtering the read can get cropped from the beginning or from the end for a specified number of bases.

[NanoFilt Website](https://gigabaseorgigabyte.wordpress.com/2017/06/05/trimming-and-filtering-oxford-nanopore-sequencing-reads/)
  
#### Centrifuge  
This tool is a microbial classification engine that enables rapid, accurate, and sensitive labeling of reads and quantification of species on desktop computers. It uses a variant of the Burroms-Wheeler transform (BTW) and the Ferrangina-Manzini (FM) index, optimized specifically for the metagenomic classification problem.

[Centrifuge repository](https://github.com/infphilo/centrifuge)  
[Centrifuge manual](https://github.com/infphilo/centrifuge/blob/master/MANUAL.markdown)

### Chiron
Provides a deep neural network base caller for nanopore sequencing. Chiron couples a convulutional neural network
with a Recurrent Neural Network and a Connectionist Temporal Classification decoder which allows translation of
raw electrical signal directly to nucleotide sequence without segmentation.  
[Chiron repository](https://github.com/haotianteng/chiron/blob/master/README.md#basecall)

Basecall  
An example call to chiron to run basecalling is:
``` bash
chiron call -i <input_fast5_folder> -o <output_folder> -m <model_folder>
```

### Krona
Krona is an interactive application that creates pie charts of hierachical data (the centrifuge output has been used as input data) 
that are multi-layered, allowing zooming in on the data. These charts are seld-contained and can be viewed with any modern web browser.
This tool has been created using KronaTools.  
[krona website](https://github.com/marbl/Krona)  

**Usage**
``` bash
ktImportTaxonomy -m 3 -t 5 centrifuge_useful.report -o ../Krona/iets.txt
```

#### Credits
This pipeline was created by:  
- Micha Snippe 385653  
- Jamie van Eijk 390103  
- Marije Hibma 380607  
- Jonathan Klimp 384092
